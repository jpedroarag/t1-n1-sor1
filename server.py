# Lida com sockets
import socket

def executar_comando(comando):
    # Lida com interações com SO
    import os

    comando = comando.decode('utf-8')
    saida = os.popen(comando).read()

    if saida == "":
        saida = "Comando inválido"
        print("%s\n" % saida)
    else:
        print("Comando executado com sucesso\n")

    saida = saida.encode()
    return saida

# Socket Internet / TCP
soquete_servidor = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = socket.gethostname()
port = 5000

soquete_servidor.bind((host, port))
soquete_servidor.listen(5)

while True:
    print("Esperando por conexão client-side...")
    soquete_cliente, endereco = soquete_servidor.accept()
    print("Conexão estabelecida com ", endereco, " com sucesso\n")

    print("Esperando por comando do cliente...")
    comando = soquete_cliente.recv(1024)
    print("Comando recebido com sucesso\n")

    print("Executando comando...")
    saida_comando = executar_comando(comando)
    soquete_cliente.send(saida_comando)

    print("Encerrando conexão com o cliente...")
    soquete_cliente.close()
    print("Conexão com ", endereco," encerrada\n")