# Escrever um programa em Python para um cliente que conecta-se
# a um servidor (echo server) solicitando que seja executado um
# comando de linha de comando (ls, dir, ping, traceroute) no servidor.
# A resposta do servidor é exibida na tela do cliente. Sugestão: utilizar
# o módulo os

from tkinter import *
import socket

def executar_comando(texto):
    # Socket tipo Internet / TCP
    soquete = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    host = socket.gethostname()
    # host = "10.112.1.75"
    port = 5000

    soquete.connect((host, port))
    comando = texto
    comando = comando.encode()

    # Cliente envia comando
    soquete.send(comando)

    # Cliente espera pela resposta servidor
    saida_do_comando = soquete.recv(1024)
    saida_do_comando = saida_do_comando.decode('utf-8')

    # Fecha a conexão
    soquete.close()
    return saida_do_comando

############################################################################

# Código de cores

# Fundo da janela - Cinza escuro
window_bg_color = "#454545"

# Maioria dos textos - Branco (tonalidade azul)
fg_color = "#FAFCFF"

# Botão de executar - Verde (não puro)
button_bg_color = "#25AA30"

############################################################################

def comando_gui(event=None):
    texto_input = entry_field.get()
    saida_do_comando = ""
    saida_label_fg = window_bg_color
    saida_label_font = ("",0,"")

    if texto_input != "":
        saida_do_comando = executar_comando(texto_input)
    else:
        saida_do_comando = "Comando inválido"

    if saida_do_comando == "Comando inválido":
        saida_label_fg = "#CC3015"
        saida_label_font = ('Calibri', 13, 'bold')

    new_window = Tk()
    new_window.title("")
    new_window.configure(background=window_bg_color)

    Label(new_window,
          text="Saída do comando \"%s\" " % texto_input,
          bg=window_bg_color,
          fg=fg_color).pack(padx=8, pady=8)

    Label(new_window,
          text=saida_do_comando,
          justify=LEFT,
          bg=fg_color,
          fg=saida_label_fg,
          font=saida_label_font).pack(padx=8, pady=8)

##########################################################################################

window = Tk()
window.title("")
window.configure(background=window_bg_color)

Label(window,
      text="Comando a ser executado no servidor",
      bg=window_bg_color,
      fg=fg_color).pack(padx=8,pady=8)

entry_field = Entry(window)
entry_field.pack(fill=X, padx=8, pady=8)
entry_field.bind('<Return>', comando_gui)

executar_button = Button(window,
                         text="Executar",
                         bg=button_bg_color,
                         fg=fg_color,
                         command=comando_gui)
executar_button.pack(padx=8, pady=8)
mainloop()